/*
 * Copyright (c) 2013, Mikhail Yevchenko. All rights reserved. PROPRIETARY/CONFIDENTIAL.
 */
package com.spark.blockchain.rpcclient;

import com.spark.blockchain.rpcclient.Bitcoin.Transaction;

/**
 *
 * @author Mikhail Yevchenko <m.ṥῥẚɱ.ѓѐḿởύḙ@azazar.com>
 */
public class SimpleBitcoinPaymentListener implements BitcoinPaymentListener {

    public void block(String blockHash) {
    }

    public void transaction(Transaction transaction) {
    }
    
}
