package com.spark.blockchain.bc.walletrpc;

import com.spark.blockchain.rpcclient.*;
import com.spark.blockchain.util.JSON;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

public class WalletApp {

    private static String url = "http://spark:123456@gcc-test.rpc.bipay.xin:80/";


    public static void main(String[] args) throws MalformedURLException, BitcoinException {
        Bitcoin bitcoin =  new BitcoinRPCClient(url);
        Bitcoin.TransactionsSinceBlock block =  bitcoin.listSinceBlock("000000bfd735ebea2185bc1a82b8844afbe036c5893c985d6655dee0608d821a");

        for (Bitcoin.Transaction transaction : block.transactions()) {
            if ("receive".equals(transaction.category())) {
                System.out.println(transaction.txId() + ":" + transaction.amount() + "=>" + transaction.address());
            }
        }
    }
}
