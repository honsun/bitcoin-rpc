package com.spark.blockchain.bc.walletrpc;

import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class DecimalTest {

    @Test
    public void testDivide(){
        BigDecimal a = new BigDecimal(18);
        BigDecimal change = new BigDecimal(-1);

        BigDecimal chg = change.divide(a, 4,RoundingMode.UP);
        System.out.println(chg);
    }
}
